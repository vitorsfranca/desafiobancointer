package com.src.interProject;

import static org.junit.Assert.assertEquals;

import java.util.LinkedList;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import com.src.interProject.dto.UniqueDigitDTO;
import com.src.interProject.service.CacheService;
import com.src.interProject.service.UniqueDigitService;

@SpringBootTest
public class CacheTests {

	@Autowired
	CacheService cacheService;
	
	@Autowired
	UniqueDigitService uniqueDigitService;
	
	private UniqueDigitDTO generateNewUniqueDigitDTO(String entry, Integer concatTimes) {
		UniqueDigitDTO dto = new UniqueDigitDTO();
		dto.setEntry(Integer.parseInt(entry));
		dto.setConcatTimes(concatTimes);
		dto.setResult(uniqueDigitService.uniqueDigit(entry, concatTimes));
		return dto;
	}
	
	@BeforeEach
	private void init() {
		cacheService.clearCache();
	}
		
	private void assertDTOS(UniqueDigitDTO expected, UniqueDigitDTO incoming) {
		assertEquals(expected.getEntry(), incoming.getEntry());
		assertEquals(expected.getResult(), incoming.getResult());
		assertEquals(expected.getConcatTimes(), incoming.getConcatTimes());
	}
	
	@Test
	public void insertFirstElementInCache() {
		UniqueDigitDTO testDTO = generateNewUniqueDigitDTO("1", 1);
		cacheService.addNewItemInCache(testDTO);
		assertEquals(2, cacheService.getCache().size());
		assertDTOS(testDTO, cacheService.getCache().get(0));
		assertDTOS(cacheService.getCache().getFirst(), cacheService.getCache().getLast());
	}

	@Test
	public void setCacheToMaximunSize() {
		LinkedList<UniqueDigitDTO> expectedCache = new LinkedList<UniqueDigitDTO>();
		for(int i = 0; i < cacheService.getCacheMaxSize(); i++) {
			UniqueDigitDTO dto = generateNewUniqueDigitDTO(Integer.toString(i), i);
			expectedCache.add(dto);
		}
		assertEquals(cacheService.getCacheMaxSize(), cacheService.getCache().size());
		for(int i = 0; i < expectedCache.size(); i++) {
			assertDTOS(expectedCache.get(i), cacheService.getCache().get(i));
		}
	}
	
	@Test
	public void rotateCacheValues() {
		LinkedList<UniqueDigitDTO> expectedCache = new LinkedList<UniqueDigitDTO>();
		int bias = 4;
		int expectedCacheSize = 10;
		for(int i = 0; i < cacheService.getCacheMaxSize() + bias; i++) {
			UniqueDigitDTO dto = generateNewUniqueDigitDTO(Integer.toString(i), i);
			expectedCache.add(dto);
		}
		assertEquals(expectedCacheSize, cacheService.getCache().size());
		for(int i = 0; i < expectedCacheSize; i++) {
			assertDTOS(expectedCache.get(i + bias), cacheService.getCache().get(i));
		}
	}
}
