package com.src.interProject;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.springframework.boot.test.context.SpringBootTest;

import com.src.interProject.dto.UserDTO;
import com.src.interProject.service.SecurityService;

@SpringBootTest

public class SecurityTests {

	@InjectMocks
	SecurityService service;
	
	String base64Key = "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEArnbl+jHELu+DEJkahXN4dVkbm7Zdn6h2HV8ugl0pght0Sml6bjPHxGKDFJsju3+mn22vP8XWzYZ3YfaeGMcvzN+aQFBX2gznsTxyjxxc+0triEOYb91V9mEX6oJ7cHeLUi2fYvCSsYErGT4LwFm2pOd5Mn5mv8VL8oIrCfSWh6hWgSMb4UcPc5IaFVjAZTLdLQFjt8K7Tsjhl3bIIbFiCufHtwYIBKWvabVe0jA4hw/4Cmu0YmtpT+qC09NcJ5t/dpjaUeX0wH17+xOnzYxBKHnwxnAY+EU73WdQXUWiKVthk54BEUJMTJJF9iUvhU8OPVaQEXkXVT+IzfJ55WUcTQIDAQAB";
	
	@Test
	void encryptUserDataFalse() {
		UserDTO dto = new UserDTO();
		dto.setEmail("email@email.com");
		dto.setName("name");
		assertFalse(service.encryptUserData(dto));
	}
	
	@Test
	void encryptUserDataTrue() {
		service.storeIncomingKey(base64Key);
		UserDTO dto = new UserDTO();
		dto.setEmail("email@email.com");
		dto.setName("name");
		assertTrue(service.encryptUserData(dto));
	}
	
	@Test
	void storeIncomingKey() {
		service.storeIncomingKey(base64Key);
		assertNotNull(service.getPubKey());
	}
}
