package com.src.interProject;

import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import com.src.interProject.dto.UniqueDigitDTO;
import com.src.interProject.entity.UniqueDigitEntity;
import com.src.interProject.repository.UniqueDigitRepository;
import com.src.interProject.service.UniqueDigitService;

@SpringBootTest
@RunWith(MockitoJUnitRunner.class)
class UniqueDigitTests {

	@InjectMocks
	UniqueDigitService service;
	
	@Autowired
	UniqueDigitService serviceInstace;
	
	@Mock
	private UniqueDigitRepository uniqueDigitRepository;
	
	private void assertDTOS(UniqueDigitDTO expected, UniqueDigitDTO incoming) {
		assertEquals(expected.getEntry(), incoming.getEntry());
		assertEquals(expected.getResult(), incoming.getResult());
		assertEquals(expected.getConcatTimes(), incoming.getConcatTimes());
	}
	
	private UniqueDigitEntity getEntity(Integer entry, Integer concatTimes, Integer result) {
		UniqueDigitEntity entity = new UniqueDigitEntity();
		entity.setEntry(entry);
		entity.setConcatTimes(concatTimes);
		entity.setResult(result);
		return entity;
	}
	
	private UniqueDigitDTO getDTO(Integer entry, Integer concatTimes, Integer result) {
		UniqueDigitDTO dto = new UniqueDigitDTO();
		dto.setEntry(entry);
		dto.setConcatTimes(concatTimes);
		dto.setResult(result);
		return dto;
	}
		
	@Test
	void testUniqueDigitSingleNumberNoConcat() {
		int result = serviceInstace.uniqueDigit("4", 1);
		assertEquals(4, result);
	}
	
	@Test
	void testUniqueDigitSingleNumber4Concats() {
		int result = serviceInstace.uniqueDigit("4", 4);
		assertEquals(16, result);
	}
	
	@Test
	void testUniqueDigitLongNumberNoConcat() {
		int result = serviceInstace.uniqueDigit("9875", 1);
		assertEquals(29, result);
	}
	
	@Test
	void testUniqueDigitLongNumber4Concats() {
		int result = serviceInstace.uniqueDigit("9875", 4);
		assertEquals(116, result);
	}
	
	@Test
	void concatNumberTest() {
		String expectedResult = "123451234512345";
		assertEquals(expectedResult, service.concatNumber("12345", 3));
	}
	
	@Test
	void mapEntityToDTOTest() {
		UniqueDigitDTO incomingDTO = service.mapEntityToDTO(getEntity(12, 2, 6));
		assertDTOS(getDTO(12, 2, 6), incomingDTO);
	}
	
	@Test
	void mapEntityListToDTOList() {
		UniqueDigitDTO incomingDTO1 = service.mapEntityToDTO(getEntity(12, 2, 6));
		UniqueDigitDTO incomingDTO2 = service.mapEntityToDTO(getEntity(4, 1, 4));
		UniqueDigitDTO incomingDTO3 = service.mapEntityToDTO(getEntity(22, 3, 18));
		assertDTOS(getDTO(12, 2, 6), incomingDTO1);
		assertDTOS(getDTO(4, 1, 4), incomingDTO2);
		assertDTOS(getDTO(22, 3, 18), incomingDTO3);
	}

	@Test
	void getUniqueDigitsFromUserFilled() {
		List<UniqueDigitEntity> expectedList = new ArrayList<UniqueDigitEntity>();
		expectedList.add(getEntity(1, 1, 1));
		expectedList.add(getEntity(2, 1, 2));
		expectedList.add(getEntity(3, 1, 4));
		expectedList.add(getEntity(4, 1, 3));
		Mockito.when(uniqueDigitRepository.getUniqueDigitsFromUser(1)).thenReturn(expectedList);
		List<UniqueDigitDTO> incomingList = service.getUniqueDigitsFromUser(1);
		assertEquals(4, incomingList.size());
	}
	
	@Test
	void getUniqueDigitsFromUserEmpty() {
		Mockito.when(uniqueDigitRepository.getUniqueDigitsFromUser(1)).thenReturn(new ArrayList<UniqueDigitEntity>());
		List<UniqueDigitDTO> incomingList = service.getUniqueDigitsFromUser(1);
		assertEquals(0, incomingList.size());
	}
	
}
