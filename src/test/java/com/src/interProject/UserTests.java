package com.src.interProject;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.boot.test.context.SpringBootTest;

import com.src.interProject.dto.UserDTO;
import com.src.interProject.entity.UserEntity;
import com.src.interProject.repository.UserRepository;
import com.src.interProject.service.SecurityService;
import com.src.interProject.service.UserService;

@SpringBootTest
@RunWith(MockitoJUnitRunner.class)
public class UserTests {
	
	@InjectMocks
	UserService service;
	
	@InjectMocks
	SecurityService securityService;
	
	@Mock
	SecurityService securityMock;
	
	@Mock
	UserRepository repository;
	
	private String mockBase64PublicKey = "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEArnbl+jHELu+DEJkahXN4dVkbm7Zdn6h2HV8ugl0pght0Sml6bjPHxGKDFJsju3+mn22vP8XWzYZ3YfaeGMcvzN+aQFBX2gznsTxyjxxc+0triEOYb91V9mEX6oJ7cHeLUi2fYvCSsYErGT4LwFm2pOd5Mn5mv8VL8oIrCfSWh6hWgSMb4UcPc5IaFVjAZTLdLQFjt8K7Tsjhl3bIIbFiCufHtwYIBKWvabVe0jA4hw/4Cmu0YmtpT+qC09NcJ5t/dpjaUeX0wH17+xOnzYxBKHnwxnAY+EU73WdQXUWiKVthk54BEUJMTJJF9iUvhU8OPVaQEXkXVT+IzfJ55WUcTQIDAQAB";
	
	private void assertDTOS(UserDTO expected, UserDTO incoming) {
		assertEquals(expected.getName(), incoming.getName());
		assertEquals(expected.getEmail(), incoming.getEmail());
	}
	
	private void assertEntities(UserEntity expected, UserEntity incoming) {
		assertEquals(expected.getId(), incoming.getId());
		assertEquals(expected.getName(), incoming.getName());
		assertEquals(expected.getEmail(), incoming.getEmail());
	}
	
	
	private UserEntity getUserEntity(Integer number) {
		UserEntity entity = new UserEntity();
		entity.setName("Name" + number);
		entity.setEmail("Email" + number);
		entity.setId(number);
		return entity;
	}
	
	private UserDTO getUserDTO(Integer number) {
		UserDTO dto = new UserDTO();
		dto.setName("Name" + number);
		dto.setEmail("Email" + number);
		return dto;
	}
		
	@Test
	void mapEntityToDTO() {
		UserDTO expectedDTO = getUserDTO(0);
		assertDTOS(expectedDTO, service.mapEntityToDTO(getUserEntity(0)));		
	}
	
	@Test
	void mapDTOToEntity() {
		UserEntity expectedEntity = getUserEntity(0);
		UserEntity incomingEntity = service.mapDTOToEntity(getUserDTO(0));
		assertEquals(expectedEntity.getEmail(), incomingEntity.getEmail());		
		assertEquals(expectedEntity.getName(), incomingEntity.getName());		
		assertNull(incomingEntity.getId());		
	}
	
	@Test
	void insertUser() {
		Mockito.when(repository.insert(Mockito.any(UserEntity.class))).thenReturn(getUserEntity(0));
		securityService.storeIncomingKey(mockBase64PublicKey);
		service.setSecurityService(securityService);
		UserDTO incomingDTO = service.insertUser(getUserEntity(0));
		assertTrue(incomingDTO.getName().length() > 0);
		assertTrue(incomingDTO.getEmail().length() > 0);
	}
	
	@Test
	void getUserById() {
		Mockito.when(repository.findById(1)).thenReturn(getUserEntity(1));
		UserDTO expectedDTO = getUserDTO(1);
		assertDTOS(expectedDTO, service.getUserById(1));
	}
	
	@Test
	void deleteUser() {
		Mockito.when(repository.delete(1)).thenReturn(true);
		assertTrue(service.deleteUser(1));
	}
	
	@Test
	void updateUser() {
		securityMock.storeIncomingKey(mockBase64PublicKey);
		service.setSecurityService(securityMock);
		Mockito.when(repository.update(Mockito.any(UserEntity.class))).thenReturn(getUserEntity(0));
		Mockito.when(securityMock.encryptUserData(Mockito.any())).thenReturn(true);
		UserDTO expectedDTO = getUserDTO(0);
		assertDTOS(expectedDTO, service.updateUser(getUserEntity(0)));
	}
}
