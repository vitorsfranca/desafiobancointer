package com.src.interProject.repository;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.src.interProject.entity.UserEntity;

@Repository
public interface UserRepository {
	public UserEntity insert(UserEntity entity);
	public UserEntity update(UserEntity entity);
	public UserEntity findById(Integer userId);
	public boolean delete(Integer userId);
	public List<UserEntity> getAll();
}
