package com.src.interProject.repository;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.src.interProject.entity.UniqueDigitEntity;

@Repository
public interface UniqueDigitRepository {
	public void insertCalculation(UniqueDigitEntity uniqueDigitEntity);
	public List<UniqueDigitEntity> getUniqueDigitsFromUser(Integer userId);
}
