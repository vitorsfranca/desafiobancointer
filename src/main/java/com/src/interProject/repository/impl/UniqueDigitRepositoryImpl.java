package com.src.interProject.repository.impl;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.src.interProject.entity.UniqueDigitEntity;
import com.src.interProject.repository.UniqueDigitRepository;

@Repository
public class UniqueDigitRepositoryImpl implements UniqueDigitRepository{

	@PersistenceContext
	protected EntityManager em;
	
	@Override
	@Transactional
	public void insertCalculation(UniqueDigitEntity uniqueDigitEntity) {
		em.persist(uniqueDigitEntity);
	}

	@Override
	public List<UniqueDigitEntity> getUniqueDigitsFromUser(Integer userId) {
		String sqlQuery = "SELECT * FROM UNIQUE_DIGIT WHERE USER_ID = " + userId;
		Query query = em.createNativeQuery(sqlQuery, UniqueDigitEntity.class);
		return (List<UniqueDigitEntity>) query.getResultList();
	}

}
