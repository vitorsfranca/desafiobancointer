package com.src.interProject.repository.impl;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.src.interProject.entity.UserEntity;
import com.src.interProject.repository.UserRepository;

import lombok.extern.java.Log;

@Repository
@Log
public class UserRepositoryImpl implements UserRepository{
	
	@PersistenceContext
	protected EntityManager em;
	
	@Override
	@Transactional
	public UserEntity insert(UserEntity entity) {
		try {
			em.persist(entity);
			return entity;
		} catch(Exception e) {
			return null;
		}
	}
	
	@Override
	@Transactional
	public UserEntity update(UserEntity entity) {
		try {
			return em.merge(entity);
		} catch (Exception e) {
			return null;
		}
	}
	
	@Override
	public UserEntity findById(Integer userId) {
		try {
			return em.find(UserEntity.class, userId);
		} catch(Exception e) {
			return null;
		}
	}
	
	@Override
	@Transactional
	public boolean delete(Integer userId) {
		try {
			UserEntity userEntity = findById(userId);
			em.remove(userEntity);
			em.flush();
			return true;
		} catch (Exception e) {
			return false;
		}
	}

	@Override
	public List<UserEntity> getAll() {
		try {
			Query query = em.createNativeQuery("SELECT * FROM USERS", UserEntity.class);
			return (List<UserEntity>) query.getResultList();
		} catch(Exception e) {
			log.severe(e.getMessage());
			return new ArrayList<>();
		}
	}	
}
