package com.src.interProject.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.src.interProject.service.SecurityService;

import io.swagger.annotations.ApiOperation;
import lombok.extern.java.Log;

@RestController
@RequestMapping(value = "security")
@Log
public class SecurityController {
	
	@Autowired
	SecurityService service;
	
	@ApiOperation(value = "Set a user public key at memory", response = Integer.class, notes = "Calculate a unique digit according to the parameters.")
	@PostMapping("keyStore")
	public ResponseEntity<String> storeUserKey(@RequestBody String incomingKey) {
		try {
			service.storeIncomingKey(incomingKey);
			return ResponseEntity.status(HttpStatus.OK).body(incomingKey);
		} catch(Exception e) {
			log.severe(e.getMessage());
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);	
		}
	}
}
