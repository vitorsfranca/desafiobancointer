package com.src.interProject.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.src.interProject.service.UniqueDigitService;
import com.src.interProject.service.UserService;

import io.swagger.annotations.ApiOperation;
import lombok.extern.java.Log;

@RestController
@RequestMapping(value = "uniquedigit")
@Log
public class UniqueDigitController {
	
	@Autowired
	UniqueDigitService service;
	
	@Autowired
	UserService userService;

	@ApiOperation(value = "Calculates unique digit by incoming values", response = Integer.class, notes = "Calculate a unique digit according to the parameters.")
	@GetMapping("{entry}/{concatTimes}")
	public ResponseEntity<Integer> calculateUniqueDigit(@PathVariable(value = "entry") String entry, @PathVariable(value = "concatTimes") int concatTimes) {
		Integer result = service.uniqueDigit(entry, concatTimes);
		if(result != null) return ResponseEntity.status(HttpStatus.OK).body(result);
		log.severe("Null result");
		return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
	}
	
	@ApiOperation(value = "Calculates unique digit by incoming values, to specific user", response = Integer.class, notes = "Calculate a unique digit according to the parameters, and stores the opearation at db.")
	@PostMapping("{entry}/{concatTimes}")
	public ResponseEntity<Integer> calculateUniqueDigitByUser(@PathVariable(value = "entry") String entry,
			@PathVariable(value = "concatTimes") int concatTimes, @RequestBody Integer userId) {
		try {
			return ResponseEntity.status(HttpStatus.OK).body(userService.insertUniqueDigit(userId, entry, concatTimes));
		} catch(Exception e) {
			log.severe(e.getMessage());
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
}
