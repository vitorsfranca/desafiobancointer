package com.src.interProject.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.src.interProject.dto.UniqueDigitDTO;
import com.src.interProject.dto.UserDTO;
import com.src.interProject.entity.UserEntity;
import com.src.interProject.service.UniqueDigitService;
import com.src.interProject.service.UserService;

import io.swagger.annotations.ApiOperation;
import lombok.extern.java.Log;

@RestController
@RequestMapping(value = "users")
@Log
public class UserController {
	
	@Autowired
	UniqueDigitService uniqueDigitService;
	
	@Autowired
	UserService userService;

	@ApiOperation(value = "Create a new User", response = UserDTO.class, notes = "Create a new user, according to the parameters.")
	@PostMapping("")
	public ResponseEntity<UserDTO> insertUser(@RequestBody UserEntity user) {
		UserDTO result = userService.insertUser(user);
		if(result != null) {
			return ResponseEntity.status(HttpStatus.OK).body(result);	
		} else {
			log.severe("Error inserting user");
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);	
		}
	}
	
	@ApiOperation(value = "Get all Users", response = UserDTO.class, notes = "Returns a list of all Users.")
	@GetMapping("")
	public ResponseEntity<List<UserDTO>> getUsersList() {
		List<UserDTO> result = userService.getUsersList();
		if(result != null) {
			return ResponseEntity.status(HttpStatus.OK).body(result);	
		} else {
			log.severe("Null list");
			return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
		}
	}
	
	@ApiOperation(value = "Get User by id", response = UserDTO.class, notes = "Get a user by id.")
	@GetMapping("{userId}")
	public ResponseEntity<UserDTO> getUserById(@PathVariable Integer userId) {
		UserDTO result = userService.getUserById(userId);
		if(result != null) {
			return ResponseEntity.status(HttpStatus.OK).body(result);	
		} else {
			log.severe("User not found");
			return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
		}
	}
	
	@ApiOperation(value = "Delete User", response = String.class, notes = "Delete a User by id")
	@DeleteMapping("{userId}")
	public ResponseEntity<String> deleteUser(@PathVariable(value = "userId") Integer userId) {
		if(userService.deleteUser(userId)) return ResponseEntity.status(HttpStatus.OK).body("User removed ok");
		log.severe("Error removing user");
		return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Error removing user");
	}
	
	@ApiOperation(value = "Update User", response = UserDTO.class, notes = "Update a User to new values.")
	@PutMapping("{userId}")
	public ResponseEntity<UserDTO> updateUser(@PathVariable(value = "userId") Integer userId, @RequestBody UserEntity user) {
		if(userId.equals(user.getId())) {
			UserDTO result = userService.updateUser(user); 
			if(result != null) {
				return ResponseEntity.status(HttpStatus.OK).body(result);
			} else {
				log.severe("User not found");
				return ResponseEntity.status(HttpStatus.NOT_FOUND).body(null);	
			}	
		}
		log.severe("Users ids dont match");
		return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(null);	
	}
	
	@ApiOperation(value = "Get User unique digits list", response = List.class, notes = "Return a list of all unique digits calculated to a specific User.")
	@GetMapping("userdigits/{userId}")
	public ResponseEntity<List<UniqueDigitDTO>> updateUser(@PathVariable(value = "userId") Integer userId) {
		List<UniqueDigitDTO> list = uniqueDigitService.getUniqueDigitsFromUser(userId);
		return ResponseEntity.status(HttpStatus.OK).body(list);
	}
}
