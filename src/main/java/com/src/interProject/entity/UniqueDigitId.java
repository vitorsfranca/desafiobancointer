package com.src.interProject.entity;

import java.io.Serializable;

public class UniqueDigitId implements Serializable {
	private Integer entry;
    private Integer concatTimes;
 
    public UniqueDigitId() {
    	super();
    }
    
    public UniqueDigitId(Integer entry, Integer concatTimes) {
        this.entry = entry;
        this.concatTimes = concatTimes;
    }
}