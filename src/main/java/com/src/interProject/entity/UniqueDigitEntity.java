package com.src.interProject.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Entity
@IdClass(UniqueDigitId.class)
@Table(name = "UNIQUE_DIGIT")
public class UniqueDigitEntity {
		
	@ManyToOne
	@JoinColumn(name = "USER_ID")
    @OnDelete(action = OnDeleteAction.CASCADE)
	private UserEntity user;
		 
	@Id
	@Column(name = "ENTRY")
  	private Integer entry;
	
	@Id
	@Column(name = "CONCAT_TIMES")
  	private Integer concatTimes;
  
	@Column(name = "RESULT")
	private Integer result;
}
