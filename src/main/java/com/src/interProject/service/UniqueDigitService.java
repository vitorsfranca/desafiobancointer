package com.src.interProject.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.src.interProject.dto.UniqueDigitDTO;
import com.src.interProject.entity.UniqueDigitEntity;
import com.src.interProject.repository.UniqueDigitRepository;

@Service
public class UniqueDigitService {
	
	@Autowired
	UniqueDigitRepository repository;
	
	@Autowired
	CacheService cacheService;
	
	public Integer uniqueDigit(String n, int k) { 
		Integer result;
		Integer nInt = Integer.parseInt(n);
		UniqueDigitDTO dataFromCache = cacheService.findUniqueDigitFromCache(nInt, k);
		if(dataFromCache != null) result = dataFromCache.getResult();
		else {
			result = uniqueDigitSum(concatNumber(n, k));
			cacheService.addNewItemInCache(new UniqueDigitDTO(nInt, k, result));
		}
		return result;
	}
	
	public String concatNumber(String n, int k) {
		try {
			String completeNumber = "";
			for(int i = 0; i < k; i++) {
				completeNumber += n;
			}
			return completeNumber;	
		} catch(Exception e) {
			return null;
		}
	}
	
	public int uniqueDigitSum(String number) {
		int result = 0;
		for(int i = 0; i < number.length(); i++) {
			result += Character.getNumericValue(number.charAt(i));
		}
		return result;
	}
	
	public UniqueDigitDTO mapEntityToDTO(UniqueDigitEntity entity) {
		UniqueDigitDTO dto = new UniqueDigitDTO();
		dto.setEntry(entity.getEntry());
		dto.setResult(entity.getResult());
		dto.setConcatTimes(entity.getConcatTimes());
		return dto;
	}
	
	public List<UniqueDigitDTO> mapEntityListToDTOList(List<UniqueDigitEntity> entityList){
		List<UniqueDigitDTO> dtoList = new ArrayList<>();
		for(UniqueDigitEntity entity : entityList) {
			dtoList.add(mapEntityToDTO(entity));
		}
		return dtoList;
	}

	public List<UniqueDigitDTO> getUniqueDigitsFromUser(Integer userId) {
		return mapEntityListToDTOList(repository.getUniqueDigitsFromUser(userId));
	}
}
