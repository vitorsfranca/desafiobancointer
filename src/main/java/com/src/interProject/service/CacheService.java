package com.src.interProject.service;

import java.util.LinkedList;

import org.springframework.stereotype.Service;

import com.src.interProject.dto.UniqueDigitDTO;

@Service
public class CacheService {
	private static LinkedList<UniqueDigitDTO> cache = new LinkedList<UniqueDigitDTO>();
	private static int CACHE_SIZE = 10;
	
	public UniqueDigitDTO findUniqueDigitFromCache(Integer n, int k) {
		for(UniqueDigitDTO currentItem : cache) {
			if(currentItem.getEntry().equals(n) && currentItem.getConcatTimes().equals(k)) return currentItem;	
		}
		return null;
	}
	
	public void addNewItemInCache(UniqueDigitDTO newItem) {
		if(cache.size() == CACHE_SIZE) {
			cache.poll();
		}
		cache.add(newItem);
	}
	
	public LinkedList<UniqueDigitDTO> getCache() {
		return cache;
	}
	
	public void clearCache() {
		cache.clear();
	}
	
	public int getCacheMaxSize() {
		return CACHE_SIZE;
	}
}
