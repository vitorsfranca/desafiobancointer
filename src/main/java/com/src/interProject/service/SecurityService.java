package com.src.interProject.service;

import java.security.KeyFactory;
import java.security.NoSuchAlgorithmException;
import java.security.PublicKey;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.X509EncodedKeySpec;
import java.util.Base64;

import javax.crypto.Cipher;

import org.springframework.stereotype.Service;

import com.src.interProject.dto.UserDTO;

@Service
public class SecurityService {
	
	private static final String RSA = "RSA";
	private PublicKey pubKey;
	
	public void storeIncomingKey(String base64PublicKey) {
        try{
            X509EncodedKeySpec keySpec = new X509EncodedKeySpec(Base64.getDecoder().decode(base64PublicKey.getBytes()));
            KeyFactory keyFactory = KeyFactory.getInstance("RSA");
            pubKey = keyFactory.generatePublic(keySpec);
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        } catch (InvalidKeySpecException e) {
            e.printStackTrace();
        }
	}
	
    public String encrypt(String data) {
    	try {
        	Cipher cipher = Cipher.getInstance("RSA");
            cipher.init(Cipher.ENCRYPT_MODE, pubKey);
            return Base64.getEncoder().encodeToString(cipher.doFinal(data.getBytes()));
    	} catch(Exception e) {
    		e.printStackTrace();
    		return null;
    	}
    }
	
	public boolean encryptUserData(UserDTO incomingData) {
		if(pubKey != null) {
			incomingData.setEmail(encrypt(incomingData.getEmail()));
			incomingData.setName(encrypt(incomingData.getName()));	
			return true;
		}
		return false;
	}
	
	public PublicKey getPubKey() {
		return pubKey;
	}
}
