package com.src.interProject.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.src.interProject.dto.UserDTO;
import com.src.interProject.entity.UniqueDigitEntity;
import com.src.interProject.entity.UserEntity;
import com.src.interProject.repository.UniqueDigitRepository;
import com.src.interProject.repository.UserRepository;

@Service
public class UserService {
	
	@Autowired
	UserRepository userRepository;
	
	@Autowired
	UniqueDigitRepository uniqueDigitRepository;
	
	@Autowired
	UniqueDigitService uniqueDigitService;
	
	@Autowired
	SecurityService securityService;
	
	public UserDTO mapEntityToDTO(UserEntity entity) {
		UserDTO dto = new UserDTO();
		dto.setEmail(entity.getEmail());
		dto.setName(entity.getName());
		return dto;
	}
	
	public UserEntity mapDTOToEntity(UserDTO dto) {
		UserEntity entity = new UserEntity();
		entity.setEmail(dto.getEmail());
		entity.setName(dto.getName());
		return entity;
	}
	
	public List<UserDTO> mapEntityListToDTOList(List<UserEntity> entityList) {
		List<UserDTO> dtoList = new ArrayList<UserDTO>();
		for(UserEntity entity : entityList) {
			dtoList.add(mapEntityToDTO(entity));
		}
		return dtoList;
	}
	
	public UserDTO insertUser(UserEntity user) {
		try {
			UserDTO dtoUser = mapEntityToDTO(user);
			if(securityService.encryptUserData(dtoUser)) {
				userRepository.insert(mapDTOToEntity(dtoUser));	
				return dtoUser;
			}
			return null;
		} catch(Exception e) {
			return null;
		}
	}
	
	public UserDTO getUserById(Integer userId) {
		UserEntity entity = userRepository.findById(userId);
		return entity != null ? mapEntityToDTO(entity) : null;
	}
	
	public boolean deleteUser(Integer userId) {
		return userRepository.delete(userId);
	}
	
	public UserDTO updateUser(UserEntity user) {
		UserDTO dtoUser = mapEntityToDTO(user);
		if(securityService.encryptUserData(dtoUser)) {
			userRepository.update(mapDTOToEntity(dtoUser));	
			return dtoUser;
		}
		return null;
	}

	public int insertUniqueDigit(Integer userId, String entry, int concatTimes) {
		int uniqueDigit = uniqueDigitService.uniqueDigit(entry, concatTimes);
		UserEntity userEntity = userRepository.findById(userId);
		UniqueDigitEntity uniqueDigitEntity = new UniqueDigitEntity();
		uniqueDigitEntity.setUser(userEntity);
		uniqueDigitEntity.setEntry(Integer.valueOf(entry));
		uniqueDigitEntity.setConcatTimes(concatTimes);
		uniqueDigitEntity.setResult(uniqueDigit);
		uniqueDigitRepository.insertCalculation(uniqueDigitEntity);
		return uniqueDigit;
	}
	
	public void setSecurityService(SecurityService securityServiceI) {
		securityService = securityServiceI;
	}

	public List<UserDTO> getUsersList() {
		return mapEntityListToDTOList(userRepository.getAll());
	}

}
