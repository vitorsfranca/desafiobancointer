package com.src.interProject;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class InterProjectApplication {

	public static void main(String[] args) {
		SpringApplication.run(InterProjectApplication.class, args);
	}

}
