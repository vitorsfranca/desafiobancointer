package com.src.interProject.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class UniqueDigitDTO {
	Integer entry;
	Integer concatTimes;
	Integer result;
	
	public UniqueDigitDTO() {
		super();
	}
	
	public UniqueDigitDTO(Integer entry, Integer concatTimes, Integer result) {
		this.entry = entry;
		this.concatTimes = concatTimes;
		this.result = result;
	}
}
