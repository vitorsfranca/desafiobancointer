package com.src.interProject.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class UserDTO {
	private String email;
	private String name;
	
	public UserDTO() {
		super();
	}
}
