# Desafio Banco Inter

Esta API foi criada como parte do desafio do Banco Inter.
Através deste projeto, é possível gerar digítos únicos, salvar usuários e consultar outros digítos previamente cadastrados.

## Instalando
Dentro da pasta raiz da aplicação, utilize o seguinte comando em seu terminal:

    mvn clean install
    
## Utilizando
Ainda dentro da pasta raiz da aplicação, utilize o seguinte comando em seu terminal:

	mvn spring-boot:run
Assim que o comando for finalizado, a API se encontrará disponível no endereço: http://localhost:8080

## Endereços
* API: http://localhost:8080
* Swagger: http://localhost:8080/swagger-ui.html#/
* H2 Console: http://localhost:8080/h2-console
	* Credenciais para o banco de dados:
		* Login: sa
		* Senha: [deixe o campo em branco]

## Conteúdos iniciais da API
A api conta com alguns dados para que seja possível realizar alguns testes na API. Vale informar que os usuários inseridos na API não possuem criptografia, visando facilitar o desenvolvimento e utilização inicial do projeto

Para realizar as inserções de usuários, vale lembrar da utilização da API para registrar a chave pública do usuário.

A utilização de IDES como Eclipse e IntelliJ é recomendada, para facilitar o entendimento do código.

## Testes
Na pasta raiz deste projeto, existe um arquivo chamado “postman_collection.json”, onde seu conteúdo é possível encontrar testes de integração de todas as rotas fornecidas pela API.
Além dos testes do postman, a API conta com testes unitários utilizando JUnit. Para rodar os testes basta rodar em seu console o comando

	mvn test

## Contato
Qualquer dúvida ou sugestão, entrar em contato através do e-mail:
vitor.willer@gmail.com
